const SSB = require('ssb-server')
const Config = require('ssb-config/inject')
const c = require('ansi-colors')
const pull = require('pull-stream')
const args = require('minimist')(process.argv.slice(2))

const fs = require('fs')

const redFile = args.a || 'red.json'
const carpeta = args.c || 'redGcc'
const red = require('./' + redFile)

const uso = `
${c.cyan('Holi!')}
Este programita va a imprimir tu feed (una cadena de mensajes que has hecho en la red)
Necesita del archivo json que ya deberias tener creado usando el programita "crearRed.js". 
Si no hay ninguna identidad creada, creara una nueva (en ese caso el feed estara vacio)

${c.cyan('Tiene dos argumentos:')}
  ${c.blue('-a para indicar el nombre del archivo donde se encuentra la identidad de la red (por defecto "red.json")')}
      ${c.cyan('ejemplo: node feed.js -a "miRed.json"')}
  ${c.blue('-c para indicar la carpeta donde se va a guardar la data de la red (por defecto redGcc)')}
      ${c.cyan('ejemplo: node feed.js -c miSuperRed')}

`

if(args.h) {
  console.log(uso)
  process.exit(1)
}

const opciones = {
  caps: red,
  path: carpeta,
  port: 9999
}

fs.existsSync(carpeta) 
  ? console.log('cargando identidad ya existente...')
  : console.log('creando nueva identidad...')

const config = Config(carpeta, opciones)

var ssb = SSB(config)

pull(
  ssb.createFeedStream(),
  pull.drain(parseMsgs, () => ssb.close())
)

function parseMsgs(msg) {
  if(msg) console.log(msg)
}
