const SSB = require('ssb-server')
const Config = require('ssb-config/inject')
const c = require('ansi-colors')
const args = require('minimist')(process.argv.slice(2))

const fs = require('fs')

const redFile = args.a || 'red.json'
const carpeta = args.c || 'redGcc'

const red = require('./' + redFile)

const uso = `
${c.cyan('Holi!')}

Este programita necesita del archivo json que ya deberias tener creado usando el programita "crearRed.js". 
Usando ese archivo creara una nueva identidad (un keypair) dentro de la red.
Esta identidad la guardara junto con su propio feed en una carpeta. Si se elimina esa carpeta y se vuelve a correr este programita se creara una nueva identidad.

${c.cyan('Tiene dos argumentos:')}
  ${c.blue('-a para indicar el nombre del archivo donde se encuentra la identidad de la red (por defecto "red.json")')}
  ${c.cyan('ejemplo: node crearRed.js -a "miRed.json"')}
  ${c.blue('-c para indicar la carpeta donde se va a guardar la data de la red (por defecto redGcc)')}
  ${c.cyan('ejemplo: node crearRed.js -c miSuperRed')}
`


if(args.h) {
  console.log(uso)
  process.exit(1)
}

const opciones = {
  caps: red,
  path: carpeta,
  port: 9999
}

fs.existsSync(carpeta) 
  ? console.log('cargando identidad ya existente...')
  : console.log('creando nueva identidad...')

const config = Config(carpeta, opciones)

var ssb = SSB(config)

ssb.whoami((err,id) => {
  if(err)console.error(err)
  console.log(`
tu identidad publica --> ${c.magenta(id.id)}

podes checkear tu identidad completa (publica,privada) en el archivo
${c.blue('secret')} dentro de la carpeta ${c.blue(carpeta)}`
  )

  ssb.close()
})
