const fs = require('fs')
const crypto = require('crypto')
const path = require('path')
const c = require('ansi-colors')
const args = require('minimist')(process.argv.slice(2))

const filename = args.a || 'red.json'
const netId = args.n || 'redGcc'

const uso = `
${c.cyan('Holi!')}

Este programita va a crear un archivo json que permite identificar una red scuttlebutt.

${c.cyan('Tiene dos argumentos opcionales:')}

  ${c.blue('-a para indicar el nombre del archivo donde se guarda la identidad de la red (por defecto "red.json")')}
  ${c.cyan('ejemplo: node crearRed.js -a "miRed.json"')}

  ${c.blue('-n un nombre para identificar dichar red (por defecto redGcc)')}
  ${c.cyan('ejemplo: node crearRed.js -n "miSuperRed"')}

`

if(args.h) {
  console.log(uso)
  process.exit(1)
}

const red = {
  shs: crypto.randomBytes(32).toString('base64'),
  sign: null,
  invite: crypto.createHash('sha256').update(netId, 'utf-8').digest('base64')
}

fs.writeFileSync(filename, JSON.stringify(red,null,2))

console.log(c.red('archivo de red creado!'), filename)
