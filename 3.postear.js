const SSB = require('ssb-server')
const Config = require('ssb-config/inject')
const c = require('ansi-colors')
const args = require('minimist')(process.argv.slice(2))

const fs = require('fs')

const redFile = args.a || 'red.json'
const carpeta = args.c || 'redGcc'

const text = args.m || 'holi 3erMundo!' //texto(si es posteo)/descripcion(si es about)
const nombre = args.n || 'persona' //Nombre. Si es about
const tipo = args.t || ( args.n ? 'about' : 'post') //El tipo de posteo (about o msg)

const red = require('./' + redFile)

const uso = `
Este programita nos permite postear mensajes en nuestra red. 
Por el momento solo soporta dos tipos de mensajes: about y post.
Necesita del archivo json que ya deberias tener creado usando el programita "crearRed.js". 
Si no hay ninguna identidad creada, creara una nueva

${c.cyan('Tiene cinco argumentos:')}
  ${c.blue('-a para indicar el nombre del archivo donde se encuentra la identidad de la red (por defecto "red.json")')}
      ${c.cyan('ejemplo: node postear.js -a "miRed.json"')}
  ${c.blue('-c para indicar la carpeta donde se va a guardar la data de la red (por defecto redGcc)')}
      ${c.cyan('ejemplo: node postear.js -c miSuperRed')}
  ${c.blue('-t para el tipo de mensaje (about o post)')}
      ${c.cyan('ejemplo: node postear.js -t "post" -m "holi gente!" ')}
  ${c.blue('-m para el texto del posteo (descripcion de la persona si es about')}
      ${c.cyan('ejemplo: node postear.js -m  "ya solito sabe que esto es un posteo"')}
  ${c.blue('-n para darle un nombre a nuestro usuario (en el caso de mensajes "about") ')}
      ${c.cyan('ejemplo: node postear.js -t "about" -n "tomas" -m "programador, musico, persona"')}

`

if(args.h) {
  console.log(uso)
  process.exit(1)
}

const opciones = {
  caps:red,
  path: carpeta,
  port: 9999
}

fs.existsSync(carpeta) 
  ? console.log('cargando identidad ya existente...')
  : console.log('creando nueva identidad...')

const config = Config(carpeta, opciones)
var ssb = SSB(config)

var posteo = {
  type: tipo
}

post()

function post(){
  if(posteo.type === 'about'){
    //armo  el objeto
    posteo.name = nombre
    posteo.description = text
    // necesito mi identidad para saber de quien estoy hablando
    ssb.whoami( (err,id) =>{
      if(err)console.error(err)
      posteo.about = id.id
      //ahora si poteo y checkeo si hay errores
      ssb.publish(posteo, (err,msg) =>{
        if(err)console.error(err)
        console.log(c.red('todo ok!'))
        console.log(msg)
        ssb.close()
      })
    })
  }else{
    posteo.text = text
    ssb.publish(posteo, (err,msg) =>{
      if(err)console.error(err)
      console.log(c.red('todo ok!'))
      console.log(msg)
      ssb.close()
    })
  }
}


