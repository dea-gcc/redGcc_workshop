# REPO PARA EL WORKSHOP DE SSB DE GRUPO COMPUTACION CRITICA

## Requerimientos:

Para correr los programitas que vienen en este proyecto, necesitas tener instalado:
  * nodejs (interprete de javascript)
  * npm (gestor de software/librerias de nodejs)

Depende de la plataforma el proceso va a ser distinto. Si se necesita, se incluiran instrucciones para instalarlo. Pero simplemente yendo a [https://nodejs.org/es/](esta pagina), deberian encontrar instaladores para nodejs (que ademas viene directamente con npm, asi que no hay que instalar dos cosas por separado) para tu plataforma. Probablemente se ofrezcan dos versiones: Actual (la mas nueva) y LTS (Long term support - un toque mas vieja, pero tambien mas estable. RECOMIENDO IR CON ESTA, PERO NO DEBERIA HABER DRAMA IGUAL).
Ambos softwares se corren dentro de una consola/terminal. 
En windows esta consola se llama cmd y en mac se llama "Terminal". Si estas en linux probablemente tengas un emulador de terminal ya instalado.
Una vez instalado testear si se instalaron bien. Esto se hace abriendo una terminal y  corriendo los siguientes comandos
```
node -v

```

```
npm -v
```

la parte "-v" simplemente sirve para checkear las versiones de cada software. Si no tira ningun error, todo deberia estar bien.

## Como correr el software

una vez descargado este proyecto (si es un .zip descomprimirlo), hay que:
1. navegar a la carpeta a traves de la terminal (o, abrir una terminal en la carpeta donde esta ubicado). En la mayoria de las terminales existe el comando "cd" para navegar por directorios. Igualmente, gxxglx es tu {yutx,amigx}.
2. instalar las librerias que necesitan los programitas para correr. Esto se logra corriendo el comando:

```
npm install
```

se creara automaticamente una nueva carpeta que se llama "node_modules"

3. Ya estas listx para correr estos programitas. El modus operandi es corriendo comandos con la forma

```
node "nombreDelProgramita.js" -opcion "valor"
```

En general no hace falta pasar ninguna opcion para que el programa funcione. Pero recomiendo siempre correr primero con la opcion "-h" para que el programa nos imprima un texto de ayuda acerca de como funciona y que opciones necesita
